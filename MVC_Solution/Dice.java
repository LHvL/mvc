package MVC_Solution;

import java.util.concurrent.ThreadLocalRandom;

public class Dice {
	private int sides;
	private int rolled;

	public Dice(int sides) {
		this.sides = sides;
	}

	public int rolling() {
		rolled = ThreadLocalRandom.current().nextInt(1, sides+1);
		return rolled;
	}
	
	
	public int getSites() {
		return sides;
	}

	public void setSites(int sides) {
		this.sides = sides;
	}

	public int getRolled() {
		return rolled;
	}

	public void setRolled(int rolled) {
		this.rolled = rolled;
	}
}
