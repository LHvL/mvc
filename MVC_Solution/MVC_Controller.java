package MVC_Solution;

public class MVC_Controller {
	private Dice player;
	private Dice opponent;
	private View theView;
	
 public MVC_Controller() {
	 player = new Dice(6);
	 opponent = new Dice(6);
	 theView = new View();
 }
	public void play() {
		theView.startGame();
		player.rolling();
		opponent.rolling();
		theView.shareResult( player.getRolled(),opponent.getRolled());
		theView.endMessage( player.getRolled() > opponent.getRolled());
	}
}
