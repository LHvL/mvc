package MVC_Solution;

public class View {
	public void startGame() {
		System.out.println("Lets roll dices, ok?");
	}

	public void shareResult(int player, int opponent) {
		System.out.println("You have rolled a " + player + " and your opponet has rolled a " + opponent + ".");
	}

	public void endMessage(boolean result) {
		System.out.println(result ? "You won" : "You lost");
	}
}
