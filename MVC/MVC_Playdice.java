package MVC;


import java.util.concurrent.ThreadLocalRandom;

public class MVC_Playdice {
	private int PlayerDice;
	private int OpponentDice;
	public void play() {
		System.out.println("Lets roll dices, ok?");
		PlayerDice = ThreadLocalRandom.current().nextInt(1, 7);
		OpponentDice = ThreadLocalRandom.current().nextInt(1, 7);
		System.out.println("You have rolled a " + PlayerDice + " and your opponet has rolled a " + OpponentDice + ".");
		if (PlayerDice > OpponentDice) {
			System.out.println("You win");
		} else{
			System.out.println("You lose"); 
		}
	} 
	public static void main(String[] args) {
		MVC_Playdice LetsHaveFun = new MVC_Playdice();
		LetsHaveFun.play();
	}
}
